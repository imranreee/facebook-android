package google_sheet_operation;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Al Imran on 28/11/2020.
 */
public class ReadGoogleSheets {

    public static String readData(Sheets sheetsService, String SPREADSHEET_ID, String sheetNum, String cellNumber) throws IOException {

        final String range = sheetNum + "!" + cellNumber;
        ValueRange result = sheetsService.spreadsheets().values().get(SPREADSHEET_ID, range).execute();
        return result.getValues().get(0).get(0).toString();
    }
}
