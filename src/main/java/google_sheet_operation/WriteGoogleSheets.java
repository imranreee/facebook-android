package google_sheet_operation;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.util.Arrays;

/**
 * Created by imran on 3/16/2018.
 */
public class WriteGoogleSheets {

    public static void writeData(Sheets sheetsService, String SPREADSHEET_ID, String sheetNum, String cellNumber, String value) throws Exception {
        ValueRange body = new ValueRange()
                .setValues(Arrays.asList(
                        Arrays.asList(value)));
        UpdateValuesResponse result = sheetsService.spreadsheets().values()
                .update(SPREADSHEET_ID, cellNumber, body)
                .setValueInputOption("RAW")
                .execute();

    }
}
