package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * This program demonstrates how to write characters to a text file.
 * @author www.codejava.net
 *
 */
public class TextFileReading {

    public static String readData(String filePath) throws Exception {

        File file = new File(filePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String data = br.readLine();
        return data;
    }

}