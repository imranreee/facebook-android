package utils;

import java.io.FileWriter;
import java.io.IOException;


/**
 * This program demonstrates how to write characters to a text file.
 * @author www.codejava.net
 *
 */
public class TextFileWriting {
    
    public static void main(String[] args) throws Exception {
        String fileLocation = System.getProperty("user.dir") +"\\txt\\adCount.txt";

        try {
            FileWriter myWriter = new FileWriter(fileLocation);
            myWriter.write("123");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        String data1 = TextFileReading.readData(fileLocation);
        System.out.println("Reading data: " +data1);
    }

}