package utils;

import com.google.api.services.sheets.v4.Sheets;
import google_sheet_operation.SheetsServiceUtil;
import google_sheet_operation.WriteGoogleSheets;
import org.testng.annotations.Test;

import java.io.File;

public class CopyImageNameToGSheet {
    private static Sheets sheetsService;
    private static String SPREADSHEET_ID = "1b-04-4VNxfB8Fkzks3Taow17BrWKkxftP7E3vskJqCk";
    private static String SHEET_NAME = "ad_details";
    @Test
    public void copyImageNameToExcel() throws Exception {
        sheetsService = SheetsServiceUtil.getSheetsService();
        String path_to_folder = System.getProperty("user.dir") +"\\images";
        File my_folder = new File(path_to_folder);
        File[] array_file = my_folder.listFiles();
        System.out.println("Total Image(s): "+array_file.length);
        for (int i = 0; i < array_file.length; i++){
            String long_file_name = array_file[i].getName();
            System.out.println(i+" >> "+long_file_name);
            WriteGoogleSheets.writeData(sheetsService, SPREADSHEET_ID, SHEET_NAME, "C"+ (i+6), long_file_name);
            Thread.sleep(1000);
        }
    }
}