package run_script;

import org.testng.TestNG;
import utils.CopyImageNameToGSheet;

public class TestRunner {
    static TestNG testNG;
    public static void main(String args[]){
        testNG = new TestNG();
        testNG.setTestClasses(new Class[] {CopyImageNameToGSheet.class});
        testNG.run();
    }
}
