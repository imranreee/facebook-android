package run_script;

import com.google.api.services.sheets.v4.Sheets;
import google_sheet_operation.ReadGoogleSheets;
import google_sheet_operation.SheetsServiceUtil;
import google_sheet_operation.WriteGoogleSheets;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.WaitOptions;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import utils.RandomNumberGenerator;
import utils.TextFileReading;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import static io.appium.java_client.touch.offset.PointOption.point;

public class Facebook_AdPost_3540022110 {
    WebDriver driver;
    String TXT_FILE_LOCATION = System.getProperty("user.dir") +"\\txt\\adCount.txt";
    String UDID = "192.168.193.133:5555";
    //String UDID = "127.0.0.1:21503";
    private static Sheets sheetsService;
    private static String SPREADSHEET_ID = "1b-04-4VNxfB8Fkzks3Taow17BrWKkxftP7E3vskJqCk";
    //private static String SPREADSHEET_ID = "1dsfnqoaQhghyVaiyddG7SA8TotoI-O8KXHekSGpX8lk";
    private static String SHEET_NAME = "Facebook_Ad";

    int ROW_NO = 5;
    int COUNTER = 1;
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;

    public Facebook_AdPost_3540022110() throws Exception {
    }

    @BeforeClass
    public void readCount() throws GeneralSecurityException, IOException {
        sheetsService = SheetsServiceUtil.getSheetsService();

        String numberOfAds = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, SHEET_NAME, "F3");

        try {
            FileWriter myWriter = new FileWriter(TXT_FILE_LOCATION);
            myWriter.write(numberOfAds);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }


    @BeforeTest
    public void runAppiumServer() throws IOException, GeneralSecurityException {
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        appiumService.clearOutPutStreams();
        System.out.println("###################################");
        System.out.println("(@_@) The BOT is ready to go (@_@)");
        System.out.println("###################################");
    }

    String numberOfAdsTxt = TextFileReading.readData(TXT_FILE_LOCATION);

    private Object[][] data = new Object[0][0];
    @DataProvider(name="NumberProvider")
    public Object[][] getNumber() {
        data = new Object[Integer.parseInt(numberOfAdsTxt)][1];
        return data;
    }

    @Test(dataProvider="NumberProvider")
    public void postAd(String args) throws Exception {
        ROW_NO += 1;
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("noReset", "true");
        dc.setCapability("autoGrantPermissions", "true");
        dc.setCapability("udid", UDID);
        dc.setCapability("appPackage", "com.google.android.apps.docs");
        dc.setCapability("appActivity", "com.google.android.apps.docs.drive.startup.StartupActivity");
        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);


        By googleDriveSearchField = By.xpath("//android.widget.TextView[@text = 'Search in Drive']");
        By googleDriveSearchField2 = By.id("search_text");
        By downloadBtn = By.xpath("//android.widget.TextView[@text = 'Download']");
        By overflowMenu2 = By.id("com.google.android.apps.docs:id/more_actions_button");


        String numberOfImages = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "B"+ ROW_NO);
        for (int k = 0; k < Integer.parseInt(numberOfImages); k++){

            String imageName = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "C"+ ROW_NO);

            Thread.sleep(1000);
            waitForVisibilityOf(googleDriveSearchField);
            driver.findElement(googleDriveSearchField).click();

            waitForVisibilityOf(googleDriveSearchField2);
            driver.findElement(googleDriveSearchField2).sendKeys(imageName);

            Thread.sleep(1000);
            ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
            Thread.sleep(1000);

            waitForVisibilityOf(overflowMenu2);
            Thread.sleep(1000);
            driver.findElement(overflowMenu2).click();

            Thread.sleep(1000);
            swipeVertical((AppiumDriver)driver,0.9,0.5,0.5,2000);
            Thread.sleep(1000);

            waitForVisibilityOf(downloadBtn);
            Thread.sleep(500);
            driver.findElement(downloadBtn).click();

            Thread.sleep(4000);
            driver.navigate().back();
        }
        driver.quit();

        DesiredCapabilities dc1 = new DesiredCapabilities();
        dc1.setCapability("platformName", "android");
        dc1.setCapability("deviceName", "OnePlusOne");
        dc1.setCapability("noReset", "true");
        dc1.setCapability("autoGrantPermissions", "true");
        dc1.setCapability("udid", UDID);
        dc1.setCapability("appPackage", "com.facebook.katana");
        dc1.setCapability("appActivity", "com.facebook.katana.LoginActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc1);


        By hamMenu = By.xpath("//android.view.View[@content-desc = 'Menu, tab 6 of 6']");
        By sellBtn = By.xpath("//android.widget.HorizontalScrollView//android.widget.Button[1]");
        By itemsForSale = By.xpath("//android.view.View[2]");

        By addImage = By.xpath("//android.widget.Button/android.view.ViewGroup");
        By nextBtn = By.xpath("//android.widget.TextView[@text = 'NEXT']");
        By nextBtn1 = By.xpath("//android.widget.LinearLayout/android.widget.Button");
        By condition = By.xpath("//android.widget.TextView[@text = 'Condition']");
        By selectCondition = By.xpath("//android.widget.TextView[@text = 'New']");
        By description = By.xpath("//android.widget.TextView[@text = 'Description']");
        By description1 = By.xpath("//android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.HorizontalScrollView[1]/android.view.ViewGroup[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.widget.EditText[1]");
        By btnPublish = By.xpath("//android.view.ViewGroup[2]/android.widget.Button");
        By inputSearch = By.xpath("//android.widget.EditText[@text = 'Search']");
        By btnApply = By.xpath("//android.view.ViewGroup[@content-desc = 'Apply']");
        By searchResult = By.xpath("//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup");

        By btnBack = By.xpath("//android.widget.ImageView[@content-desc=\"Back\"]");
        By btnDiscard = By.id("android:id/button2");
        By locationField = By.xpath("//android.widget.TextView[@text = 'Location']");

        String imagesArray[] = new String[4];
        imagesArray[0] = "//android.widget.ImageView[2]//android.widget.ImageView";
        imagesArray[1] = "//android.widget.ImageView[3]//android.widget.ImageView";
        imagesArray[2] = "//android.view.ViewGroup[2]/android.widget.ImageView[1]//android.widget.ImageView";
        imagesArray[3] = "//android.view.ViewGroup[2]/android.widget.ImageView[2]//android.widget.ImageView";
        By titleField = By.xpath("//android.widget.TextView[@text = 'Title']");
        By titleField1 = By.xpath("//android.view.ViewGroup[5]/android.widget.EditText");
        By priceField = By.xpath("//android.widget.TextView[@text = 'Price']");
        By priceField1 = By.xpath("//android.view.ViewGroup[6]/android.widget.EditText");

        waitForClickAbilityOf(hamMenu);
        new TouchAction((AndroidDriver)driver).tap(point(420, 195)).perform();

        waitForClickAbilityOf(sellBtn);
        driver.findElement(sellBtn).click();

        waitForClickAbilityOf(itemsForSale);
        Thread.sleep(1000);
        driver.findElement(itemsForSale).click();

        /*if (COUNTER == 1){
            waitForClickAbilityOf(btnBack);
            driver.findElement(btnBack).click();

            Thread.sleep(2000);
            if (driver.findElements(btnDiscard).size() > 0){
                waitForClickAbilityOf(btnDiscard);
                driver.findElement(btnDiscard).click();
            }

            Thread.sleep(2000);
            if (driver.findElements(btnDiscard).size() > 0){
                waitForClickAbilityOf(btnDiscard);
                driver.findElement(btnDiscard).click();
            }
            waitForClickAbilityOf(sellBtn);
            driver.findElement(sellBtn).click();

            waitForClickAbilityOf(itemsForSale);
            Thread.sleep(1000);
            driver.findElement(itemsForSale).click();

            COUNTER += 1;
        }*/

        waitForClickAbilityOf(btnBack);
        driver.findElement(btnBack).click();

        Thread.sleep(2000);
        if (driver.findElements(btnDiscard).size() > 0){
            waitForClickAbilityOf(btnDiscard);
            driver.findElement(btnDiscard).click();
        }

        Thread.sleep(2000);
        if (driver.findElements(btnDiscard).size() > 0){
            waitForClickAbilityOf(btnDiscard);
            driver.findElement(btnDiscard).click();
        }
        waitForClickAbilityOf(sellBtn);
        driver.findElement(sellBtn).click();

        waitForClickAbilityOf(itemsForSale);
        Thread.sleep(1000);
        driver.findElement(itemsForSale).click();

        waitForClickAbilityOf(addImage);
        driver.findElement(addImage).click();

        Thread.sleep(3000);
        for (int i = 0; i < Integer.parseInt(numberOfImages); i++){
            driver.findElement(By.xpath(imagesArray[i])).click();
        }

        waitForClickAbilityOf(nextBtn);
        driver.findElement(nextBtn).click();

        waitForClickAbilityOf(titleField);
        driver.findElement(titleField).click();
        String productTitle = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "G"+ ROW_NO);
        driver.findElement(titleField1).sendKeys(productTitle);

        waitForClickAbilityOf(priceField);
        driver.findElement(priceField).click();
        String priceValue = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "H"+ ROW_NO);
        driver.findElement(priceField1).sendKeys(priceValue);

        Thread.sleep(1000);
        swipeVertical((AppiumDriver)driver,0.9,0.1,0.5,3000);
        Thread.sleep(1000);

        driver.findElement(condition).click();
        waitForVisibilityOf(selectCondition);
        driver.findElement(selectCondition).click();

        Thread.sleep(2000);
        swipeVertical((AppiumDriver)driver,0.9,0.1,0.03,3000);
        Thread.sleep(1000);
        swipeVertical((AppiumDriver)driver,0.9,0.1,0.03,3000);
        Thread.sleep(1000);


        waitForClickAbilityOf(locationField);
        driver.findElement(locationField).click();

        waitForClickAbilityOf(inputSearch);
        driver.findElement(inputSearch).click();
        String location1 = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "J"+ ROW_NO);
        driver.findElement(inputSearch).sendKeys(location1+"0");

        Thread.sleep(1000);
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.DEL));

        Thread.sleep(1000);
        waitForVisibilityOf(searchResult);
        driver.findElement(searchResult).click();
        waitForClickAbilityOf(btnApply);
        driver.findElement(btnApply).click();

        waitForClickAbilityOf(description);
        driver.findElement(description).click();
        String txtDescription = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "I"+ ROW_NO);
        driver.findElement(description1).sendKeys(txtDescription);

        waitForClickAbilityOf(nextBtn1);
        driver.findElement(nextBtn1).click();

        waitForClickAbilityOf(btnPublish);
        driver.findElement(btnPublish).click();

        Thread.sleep(3000);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println((ROW_NO - 5)+" no ad posted successfully at: "+formatter.format(date));

        WriteGoogleSheets.writeData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "K"+ ROW_NO, "Posted");

        String startingValueOfRandom = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "D3");
        String endValueOfRandom = ReadGoogleSheets.readData(sheetsService, SPREADSHEET_ID, "Facebook_Ad", "E3");
        int intervalTime = (1000) * RandomNumberGenerator.getRandomNumber(Integer.parseInt(startingValueOfRandom), Integer.parseInt(endValueOfRandom));

        System.out.println("Waiting time "+intervalTime/(1000)+" seconds, before posting next ad!");
        Thread.sleep(intervalTime);

        driver.quit();
    }

    @AfterTest
    public void endBot() {
        appiumService.stop();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForClickAbilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }


    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(point(anchor, endPoint)).release().perform();
    }

}
